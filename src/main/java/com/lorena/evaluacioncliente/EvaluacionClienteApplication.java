package com.lorena.evaluacioncliente;

import com.lorena.evaluacioncliente.entity.catalogo.Libro;
import com.lorena.evaluacioncliente.entity.config.RequestMap;
import com.lorena.evaluacioncliente.entity.config.Role;
import com.lorena.evaluacioncliente.entity.config.User;
import com.lorena.evaluacioncliente.repository.catalogo.LibroRepository;
import com.lorena.evaluacioncliente.repository.config.RequestMapRepository;
import com.lorena.evaluacioncliente.repository.config.RoleRepository;
import com.lorena.evaluacioncliente.repository.config.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

@SpringBootApplication
@AllArgsConstructor
public class EvaluacionClienteApplication implements CommandLineRunner {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final RequestMapRepository requestMapRepository;
    private final PasswordEncoder passwordEncoder;
    private final LibroRepository libroRepository;

    @Override
    public void run(String... args) throws Exception {
        addRequestMap();

        if (!roleRepository.findByAuthority("ROLE_ANT_ROOT").isPresent()) {
            Role adminRole = roleRepository.save(new Role("ROLE_ANT_ROOT", "Role to root user", "ROLE_ANT_ROOT"));

            HashSet<Role> roles = new HashSet<>();
            roles.add(adminRole);

            String password = passwordEncoder.encode("L0r3n4v#");
            userRepository.save(new User("Administrador", "admin", "lorena@gmail.com", password, password, roles));

            HashSet<RequestMap> requestMaps = new HashSet<>();
            requestMaps.addAll(requestMapRepository.findAllById(List.of(1l, 3l))); // pokemon, encriptar
            Role role = roleRepository.saveAndFlush(new Role("USER", "Role to user", "USER", requestMaps));

            roles = new HashSet<>();
            roles.add(role);

            password = passwordEncoder.encode("us3Rv#");
            userRepository.save(new User("Pokemon", "pokemon", "pokemon@gmail.com", password, password, roles));

            addLibros();
        }
    }

    private void addLibros() {
        libroRepository.save(new Libro("¡Gracias!", "Andrés Manuel López Obrador", 560, true));
        libroRepository.save(new Libro("El fotógrafo de Auschwitz", "Maurizio Onnis", 296, true));
        libroRepository.save(new Libro("Fuera del agua. Un giro inesperado", "Liz Braswell", 400, true));
        libroRepository.save(new Libro("Quiero comerme tu páncreas", "Yoru Sumino", 288, true));
        libroRepository.save(new Libro("El pacto secreto", "Judy Rakowsky", 328, true));
        libroRepository.save(new Libro("Sé más Capitana Marvel", "Marvel", 64, true));
        libroRepository.save(new Libro("Las hijas de la criada", "Sonsoles Ónega", 480, true));
        libroRepository.save(new Libro("Encuentros en Bonaval", "Sonsoles Ónega", 620, true));
        libroRepository.save(new Libro("La sociedad de la nieve", "Pablo Vierci", 1024, true));
        libroRepository.save(new Libro("Cuatro veranos", "Benito Taibo", 152, true));
        libroRepository.save(new Libro("Verdad o reto", "Camilla Läckberg", 136, false));
    }

    public void addRequestMap() {
        try {
            ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
            scanner.addIncludeFilter(new AnnotationTypeFilter(RequestMapping.class));

            List<String> tipoSeguridad = new ArrayList<>();
            tipoSeguridad.add("permitAll");
            tipoSeguridad.add("isAuthenticated()");

            for (BeanDefinition beanDefinition : scanner.findCandidateComponents("com.lorena.evaluacioncliente.controller")) {
                Class classNew = Class.forName(beanDefinition.getBeanClassName());

                String pathPrincipal = (classNew.getAnnotation(RequestMapping.class) != null) ?
                        ((RequestMapping) classNew.getAnnotation(RequestMapping.class)).value()[0].toString() : "";

                Arrays.stream(classNew.getMethods()).filter(it ->
                        (it.getAnnotation(GetMapping.class) != null || it.getAnnotation(PostMapping.class) != null ||
                                it.getAnnotation(PutMapping.class) != null || it.getAnnotation(PatchMapping.class) != null ||
                                it.getAnnotation(DeleteMapping.class) != null)
                        && (it.getAnnotation(Secured.class) != null && !tipoSeguridad.contains(((Secured) it.getAnnotation(Secured.class)).value()[0]))
                ).forEach(it -> {
                    String uri = pathPrincipal;
                    String metodoHttp = "";

                    if (it.getAnnotations()[0] instanceof GetMapping) {
                        GetMapping mapping = it.getAnnotation(GetMapping.class);
                        uri += mapping.value()[0];
                        metodoHttp = "GET";
                    } else if (it.getAnnotations()[0] instanceof PostMapping) {
                        PostMapping mapping = it.getAnnotation(PostMapping.class);
                        uri += mapping.value()[0];
                        metodoHttp = "POST";
                    } else if (it.getAnnotations()[0] instanceof PutMapping) {
                        PutMapping mapping = it.getAnnotation(PutMapping.class);
                        uri += mapping.value()[0];
                        metodoHttp = "PUT";
                    } else if (it.getAnnotations()[0] instanceof PatchMapping) {
                        PatchMapping mapping = it.getAnnotation(PatchMapping.class);
                        uri += mapping.value()[0];
                        metodoHttp = "PATCH";
                    } else if (it.getAnnotations()[0] instanceof DeleteMapping) {
                        DeleteMapping mapping = it.getAnnotation(DeleteMapping.class);
                        uri += mapping.value()[0];
                        metodoHttp = "DELETE";
                    }

                    RequestMap requestMapFind = requestMapRepository.findByUrlAndControllerAndActionAndMethodHttp(uri, classNew.getSimpleName().toString(), it.getName(), metodoHttp).orElse(new RequestMap());
                    if (requestMapFind.getId() == null) {
                        RequestMap requestMap = new RequestMap(uri, classNew.getSimpleName().toString(), it.getName(), metodoHttp);
                        requestMapRepository.save(requestMap);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(EvaluacionClienteApplication.class, args);
    }

}
