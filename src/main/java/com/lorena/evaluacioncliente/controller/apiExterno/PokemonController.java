package com.lorena.evaluacioncliente.controller.apiExterno;

import com.lorena.evaluacioncliente.service.apiExterno.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Contiene endpoints para consumir el servicio externo de pokemon
 *
 * @author lorenav
 */
@RestController
@RequestMapping("/pokemon")
public class PokemonController {

    @Autowired
    PokemonService pokemonService;

    /**
     * Obtiene una lista de información de pokemon
     *
     * @return Mapa de objetos contenidos en el servicio externo
     */
    @GetMapping(value = "/ditto")
    public Map<Object, Object> getDitto() {
        return pokemonService.getPokemonDitto();
    }
}
