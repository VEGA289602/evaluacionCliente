package com.lorena.evaluacioncliente.controller.utilities;

import com.lorena.evaluacioncliente.service.utilities.AESEncriptarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Contiene funciones independientes, las cuales realizan tareas específicas para la construcción de otros componentes
 *
 * @author lorenav
 */
@RestController
@RequestMapping("/utilerias")
public class UtileriasController {

    @Autowired
    AESEncriptarService aesEncriptarService;

    /**
     * Realiza la encriptación de una cadena bajo el modelo de AES
     *
     * @param cadena Cadena la cual se va a encriptar
     * @return Cadena modificada y encriptada
     */
    @GetMapping("/encriptar")
    public String encriptar(@RequestParam String cadena) {
        return aesEncriptarService.encriptar(cadena);
    }

    /**
     * Realiza la desencriptación de una cadena bajo el modelo de AES
     *
     * @param cadena Cadena la cual se va a desencriptar
     * @return Cadena original antes de encriptación
     */
    @GetMapping("/desencriptar")
    @Secured("isAuthenticated()")
    public String desencriptar(@RequestParam String cadena) {
        return aesEncriptarService.desencriptar(cadena);
    }

}
