package com.lorena.evaluacioncliente.controller.config;


import com.lorena.evaluacioncliente.dto.request.LoginReqDto;
import com.lorena.evaluacioncliente.dto.response.LoginDto;
import com.lorena.evaluacioncliente.service.config.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * Contiene endpoints para la autenticación del usuario
 *
 * @author lorenav
 */
@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
public class AuthController {

    @Autowired
    private AuthService authService;

    /**
     * Valida las credenciales enviadas para una autorización de uso de los enpoints del sistema
     *
     * @param body Parámetros [username, email] para la validación de inicio de sesión
     * @return Respuesta a la validación de login
     */
    @PostMapping("/login")
    @Secured("permitAll")
    public LoginDto login(@RequestBody LoginReqDto body) {
        return authService.loginUser(body.getUsername(), body.getPassword());
    }
}
