package com.lorena.evaluacioncliente.controller.catalogo;

import com.lorena.evaluacioncliente.entity.catalogo.Libro;
import com.lorena.evaluacioncliente.service.catalogo.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Contiene endpoints para la administración del catálogo de Libro
 *
 * @author lorenav
 */
@RestController
@RequestMapping("/libro")
public class LibroController {

    @Autowired
    private LibroService libroService;

    /**
     * Obtiene una lista de libros dependiendo los filtros de búsqueda
     *
     * @param parametros Mapa con los parámetros enviados en la solicitud
     * @return Lista de libros filtrados por los parámetros de búsqueda
     */
    @GetMapping(value = "/")
    public List<Libro> consultar(@RequestParam Map<String, String> parametros) {
        return libroService.consultar(parametros);
    }
}
