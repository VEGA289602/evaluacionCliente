package com.lorena.evaluacioncliente.repository.catalogo;

import com.lorena.evaluacioncliente.entity.catalogo.Libro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Contiene endpoints para la administración del catálogo de Libro
 *
 * @author lorenav
 */
@Repository
public interface LibroRepository extends JpaRepository<Libro, Long> {

}
