package com.lorena.evaluacioncliente.repository.config;

import com.lorena.evaluacioncliente.entity.config.RequestMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Contiene endpoints para la administración de la entidad RequestMap
 *
 * @author lorenav
 */
@Repository
public interface RequestMapRepository extends JpaRepository<RequestMap, Long> {
    Optional<RequestMap> findByUrlAndControllerAndActionAndMethodHttp(String url, String controller, String action, String methodHttp);

    @Query(value = "SELECT rm.* FROM usuario u " +
            "INNER JOIN user_role ur ON ur.user_id = u.id " +
            "INNER JOIN role r ON r.id = ur.role_id " +
            "INNER JOIN role_url_mapping rum ON rum.role_id = r.id " +
            "INNER JOIN request_map rm ON rm.id = rum.request_map_id " +
            "WHERE rm.url = :url AND u.username = :user AND r.authority IN :roles AND rm.method_http = :method", nativeQuery = true)
    Optional<RequestMap> findRequestMapByUserAndRoleListAndUrlAndMethodHttp(@Param("user") String user,
                                                                            @Param("roles") List<String> roles,
                                                                            @Param("url") String url,
                                                                            @Param("method") String methodHttp);
}
