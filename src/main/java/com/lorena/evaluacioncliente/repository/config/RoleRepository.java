package com.lorena.evaluacioncliente.repository.config;

import com.lorena.evaluacioncliente.entity.config.RequestMap;
import com.lorena.evaluacioncliente.entity.config.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Contiene endpoints para la administración de la entidad Role
 *
 * @author lorenav
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByAuthority(String authority);
    List<RequestMap> findUrlMappingByAuthority(String authority);
}
