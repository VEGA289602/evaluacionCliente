package com.lorena.evaluacioncliente.repository.config;

import com.lorena.evaluacioncliente.entity.config.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Contiene endpoints para la administración de la entidad User
 *
 * @author lorenav
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameOrEmail(String username, String email);
}
