package com.lorena.evaluacioncliente.utilities;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * Componente que genera llaves RSA
 *
 * @author lorenav
 */
@Data
@Component
public class RSAKeyProperties {

    private RSAPublicKey publicKey = null;
    private RSAPrivateKey privateKey = null;

    /**
     * Constructor de la clase
     * Inicializa las llaves de RSA
     */
    public RSAKeyProperties() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            publicKey = (RSAPublicKey) keyPair.getPublic();
            privateKey = (RSAPrivateKey) keyPair.getPrivate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
