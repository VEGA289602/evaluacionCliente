package com.lorena.evaluacioncliente.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contiene los atributos esperados en el request de inicio de sesión
 *
 * @author lorenav
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginReqDto {
    private String username = "";
    private String password = "";
    private String email = "";
}
