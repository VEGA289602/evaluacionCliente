package com.lorena.evaluacioncliente.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contiene los atributos esperados en el response de inicio de sesión
 *
 * @author lorenav
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {
    private UserJwtDto user = null;
    @JsonProperty("access_token")
    private String accessToken = null;
    @JsonProperty("refresh_token")
    private String refreshToken = null;
    @JsonProperty("expired_in")
    private String expiredIn = null;
    @JsonProperty("refresh_expired_in")
    private String refreshExpiredIn = null;
}
