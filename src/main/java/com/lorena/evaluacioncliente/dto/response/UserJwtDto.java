package com.lorena.evaluacioncliente.dto.response;

import com.lorena.evaluacioncliente.entity.config.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Contiene los atributos esperados en el response de inicio de sesión
 *
 * @author lorenav
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserJwtDto {
    Boolean accountExpired = false;
    Boolean accountLocked = false;
    Boolean enabled = true;
    Boolean passwordExpired = false;

    String username = "";
    String name = null;
    String lastName = null;
    String motherLastName = null;

    public UserJwtDto(User user) {
        this.accountExpired = user.isAccountNonExpired();
        this.accountLocked = user.isAccountNonLocked();
        this.enabled = user.isEnabled();
        this.passwordExpired = user.isCredentialsNonExpired();
        this.username = user.getUsername();
        this.name = user.getName();
        this.lastName = user.getLastName();
        this.motherLastName = user.getMotherLastName();
    }
}
