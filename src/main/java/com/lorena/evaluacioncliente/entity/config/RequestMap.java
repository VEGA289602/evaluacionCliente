package com.lorena.evaluacioncliente.entity.config;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contiene la información de la tabla request_map de la base de datos
 *
 * @author lorenav
 */
@Entity
@Table(name = "request_map")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestMap {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String url;

    @Column
    private String controller;

    @Column
    private String action;

    @Column(name = "method_http")
    private String methodHttp;

    public RequestMap(String url, String controller, String action, String methodHttp) {
        this.url = url;
        this.controller = controller;
        this.action = action;
        this.methodHttp = methodHttp;
    }
}
