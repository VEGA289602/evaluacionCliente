package com.lorena.evaluacioncliente.entity.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Contiene la información de la tabla usuario de la base de datos
 *
 * @author lorenav
 */
@Entity(name = "User")
@Table(name = "usuario")
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "id")
public class User extends Person implements UserDetails {

    @Column
    Boolean accountExpired = false;

    @Column
    Boolean accountLocked = false;

    @Column
    Boolean enabled = true;

    @Column
    Boolean passwordExpired = false;

    @Column
    Date passwordChangeDate = new Date();

    @Column
    String username = "";

    @Column
    @JsonIgnore
    String password = "";

    @Column
    @JsonIgnore
    String lastPassword = "";

    @Column
    String avatarProfile = "";
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    Set<Role> authorities = null;

    public User(String name, String username, String email, String password, String lastPassword, Set<Role> authorities) {
        super();
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.lastPassword = lastPassword;
        this.authorities = authorities;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return !this.accountExpired;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return !this.accountLocked;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return !this.passwordExpired;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
