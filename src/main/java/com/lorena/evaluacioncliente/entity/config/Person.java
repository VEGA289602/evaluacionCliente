package com.lorena.evaluacioncliente.entity.config;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

/**
 * Contiene la información de la tabla persona de la base de datos
 *
 * @author lorenav
 */
@Table(name = "persona")
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    Long id;

    @Column
    String name;

    @Column
    String lastName;

    @Column
    String motherLastName;

    @Column
    String email;
}
