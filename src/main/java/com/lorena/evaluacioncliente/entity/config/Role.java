package com.lorena.evaluacioncliente.entity.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.HashSet;
import java.util.Set;

/**
 * Contiene la información de la tabla role de la base de datos
 *
 * @author lorenav
 */
@Entity
@Table(name = "role")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String authority;

    @JsonIgnore
    String description;

    @JsonIgnore
    String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_url_mapping", joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "request_map_id", referencedColumnName = "id")})
    @JsonIgnore
    Set<RequestMap> urlMapping = new HashSet<>();

    public Role(String authority, String description, String name) {
        this.authority = authority;
        this.description = description;
        this.name = name;
    }

    public Role(String authority, String description, String name, Set<RequestMap> urlMapping) {
        this.name = name;
        this.authority = authority;
        this.urlMapping = urlMapping;
        this.description = description;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
