package com.lorena.evaluacioncliente.entity.catalogo;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contiene la información de la tabla libro de la base de datos
 *
 * @author lorenav
 */
@Entity
@Table(name = "libro")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Libro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "titulo")
    String titulo;

    @Column(name = "autor")
    String autor;

    @Column(name = "paginas")
    Integer paginas;

    @Column(name = "estatus")
    Boolean estatus;

    public Libro(String titulo, String autor, Integer paginas, Boolean estatus) {
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
        this.estatus = estatus;
    }
}
