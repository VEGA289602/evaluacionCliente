package com.lorena.evaluacioncliente.config;

import com.lorena.evaluacioncliente.config.jwt.AuthEntryPoint;
import com.lorena.evaluacioncliente.config.jwt.AuthFilter;
import com.lorena.evaluacioncliente.config.jwt.PocClosureVoter;
import com.lorena.evaluacioncliente.utilities.RSAKeyProperties;


import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * Configuración de spring security para las solicitudes recibidas en el api
 *
 * @author lorenav
 */
@Configuration
public class SecurityConfig {

    private RSAKeyProperties keys;

    @Value("${evaluacion-cliente.openapi.dev-url}")
    private String urlDeveloper;

    @Value("${evaluacion-cliente.openapi.prod-url}")
    private String urlProduccion;

    @Autowired
    private AuthEntryPoint authEntryPoint;

    /**
     * Constructor que inicializa las variables de clase
     *
     * @param keys Componente con llaves RSA
     */
    public SecurityConfig(RSAKeyProperties keys) {
        this.keys = keys;
    }

    /**
     * Proporciona el recurso para la autenticación de accesos
     *
     * @return Clase que determina si la solicititud es autenticado para permitir el acceso a algún recurso
     */
    @Bean
    public PocClosureVoter autorizationManager() {
        return new PocClosureVoter();
    }

    /**
     * Proporciona el recurso para codificar contraseñas
     *
     * @return Clase que permite codificar contraseñas
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Proporciona el recurso para la autenticación de un token por medio de filtros
     *
     * @return Clase que filtra las solicitudes HTTP el cual solo se invocará una sola vez por solicitud
     */
    @Bean
    public AuthFilter authenticationJwtTokenFilter() {
        return new AuthFilter();
    }

    /**
     * Proporciona el recurso de manejador de autorizaciones de las solicitudes HTTP
     *
     * @param detailsService Clase que almacena los datos de inicio de sesión proporcionados por el usuario
     * @return Clase manejador de autorizaciones
     */
    @Bean
    public AuthenticationManager authManager(UserDetailsService detailsService) {
        DaoAuthenticationProvider daoProvider = new DaoAuthenticationProvider();
        daoProvider.setUserDetailsService(detailsService);
        daoProvider.setPasswordEncoder(passwordEncoder());
        return new ProviderManager(daoProvider);
    }

    /**
     * Filtro de solicitudes con políticas específicas para el uso del recurso de la api
     *
     * @param http Clase para configurar las solicirtudes HTTP
     * @return Clase que proteje el recurso de la aplicación
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf(csrf -> csrf.disable())
                .authorizeHttpRequests(auth -> {
                    auth.requestMatchers("/auth/**").permitAll();
                    auth.requestMatchers("/utilerias/desencriptar").authenticated();
                    auth.anyRequest().access(autorizationManager());
                })
                .exceptionHandling(excep -> {
                    excep.authenticationEntryPoint(authEntryPoint);
                })
                .oauth2ResourceServer(oauth2 -> {
                    oauth2.jwt(jwt -> {
                        jwt.jwtAuthenticationConverter(jwtAuthenticationConverter());
                    });
                })
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement(session -> {
                    session.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                });
        return http.build();
    }

    /**
     * Proporciona el recurso para decodificar la llave pública
     *
     * @return Clase responsable de decodificar un json web token
     */
    @Bean
    public JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withPublicKey(this.keys.getPublicKey()).build();
    }

    /**
     * Proporciona el recurso para codificar las llaves de RSA para generar el json web token
     *
     * @return Clase responsable de codificar un json web token
     */
    @Bean
    public JwtEncoder jwtEncoder() {
        JWK jwk = new RSAKey.Builder(keys.getPublicKey()).privateKey(keys.getPrivateKey()).build();
        JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
        return new NimbusJwtEncoder(jwks);
    }

    /**
     * Configura un conversor de Authorities, para establecerlos en el contexto de autenticación
     *
     * @return Clase que establece los roles de autenticación
     */
    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("");
        JwtAuthenticationConverter jwtConverter = new JwtAuthenticationConverter();
        jwtConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return jwtConverter;
    }
}
