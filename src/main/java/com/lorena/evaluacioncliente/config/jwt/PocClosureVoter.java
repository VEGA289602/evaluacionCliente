package com.lorena.evaluacioncliente.config.jwt;

import com.lorena.evaluacioncliente.entity.config.RequestMap;
import com.lorena.evaluacioncliente.repository.config.RequestMapRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

/**
 * Determina si la solicititud es autenticado para permitir el acceso a algún recurso
 *
 * @author lorenav
 */
@Component
public final class PocClosureVoter implements AuthorizationManager<RequestAuthorizationContext> {

    @Autowired
    private RequestMapRepository requestMapRepository;

    /**
     * Verifica que la autorización encontrada en una solicitud tenga acceso al recurso solicitado
     *
     * @param authentication Autenticación del usuario
     * @param object         Obtiene el contexto de la autorización de una conexión específicada
     * @return Concede o deniega el acceso a la solicitud enviada
     */
    @Override
    public AuthorizationDecision check(Supplier<Authentication> authentication, RequestAuthorizationContext object) {
        String urlRequested = object.getRequest().getRequestURI().toString();
        Collection<GrantedAuthority> userRoles = (Collection<GrantedAuthority>) authentication.get().getAuthorities();
        String requestMethod = object.getRequest().getMethod();
        List<String> rolesBD = userRoles.stream().map(it -> it.getAuthority()).toList();

        RequestMap requestMap = requestMapRepository.findRequestMapByUserAndRoleListAndUrlAndMethodHttp(
                authentication.get().getName(), rolesBD, urlRequested, requestMethod
        ).orElse(new RequestMap());

        boolean accessValidate = (rolesBD.contains("ROLE_ANT_ROOT") || (requestMap.getId() != null && requestMap.getId() != 0L));
        return new AuthorizationDecision(accessValidate);
    }
}
