package com.lorena.evaluacioncliente.config.jwt;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Modifica el encabezado según HttpServletResponse para iniciar el proceso de autenticación
 *
 * @author lorenav
 */
@Component
public class AuthEntryPoint implements AuthenticationEntryPoint {

    /**
     * Modifica el encabezado para prepararlo para el comienzo de la autenticación
     *
     * @param request       Proporciona la información de las solicitudes HTTP
     * @param response      Proporciona funcionalidad específica de HTTP para enviar una respuesta
     * @param authException Porporciona las excepciones relacionadas con un proceso de Autenticcación
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized");
    }
}
