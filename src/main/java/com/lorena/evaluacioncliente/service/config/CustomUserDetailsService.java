package com.lorena.evaluacioncliente.service.config;

import com.lorena.evaluacioncliente.repository.config.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Clase que implementa los datos de inicio de sesión proporcionados por el usuario
 *
 * @author lorenav
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    /**
     * Consulta el usuario por medio de su username y determina si exite o no el UserDetails
     *
     * @param username Clavede usuario
     * @return Clase que almacena las credenciales del usuario
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException("User does not exist"));
    }
}
