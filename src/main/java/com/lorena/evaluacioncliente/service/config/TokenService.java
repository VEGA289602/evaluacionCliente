package com.lorena.evaluacioncliente.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.sql.DriverManager.println;

/**
 * Contiene las funciones para la validación y generacion de json web token
 *
 * @author lorenav
 */
@Service
public class TokenService {
    @Autowired
    private JwtEncoder jwtEncoder;

    @Autowired
    private JwtDecoder jwtDecoder;

    /**
     * Generación de json web token en base a una autorización
     *
     * @param auth Entidad principal autenticada
     * @return Cadena con el token de autenticación
     */
    public String generateJwt(Authentication auth) {

        long expire = 3600;
        String scope = auth.getAuthorities().stream()
                .map(ma -> ma.getAuthority())
                .collect(Collectors.joining(" "));

        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(Instant.now())
                .subject(auth.getName())
                .claim("roles", scope.split(" "))
                .claim("typ", "Bearer")
                .expiresAt(Instant.now().plusSeconds(expire))
                .id(UUID.randomUUID().toString())
                .build();

        return jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    /**
     * Obtiene el clave de usuario de un json web token
     *
     * @param token Cadena con el token
     * @return Cadena con el clave de usuario
     */
    public String getUserNameFromJwtToken(String token) {
        return jwtDecoder.decode(token).getSubject();
    }

    /**
     * Valida que el token sea correcto
     *
     * @param authToken Cadena con el token
     * @return [true: token válido, false: token invalido]
     */
    public Boolean validateJwtToken(String authToken) {
        try {
            Jwt jwt = jwtDecoder.decode(authToken);
            return true;
        } catch (Exception ignore) {
        }
        return false;
    }

    /**
     * Valida que el token sea de autenticadción Bearer
     *
     * @param token Cadena con el json web token
     * @return [true: el token si contiene Bearer, false: la cadena no contiene Bearer]
     */
    public Boolean isBearer(String token) {
        Jwt decoder;
        String type = "";
        try {
            decoder = jwtDecoder.decode(token);
            type = decoder.getClaims().get("type").toString();
        } catch (Exception ignore) {
        }
        return type != null && type.equals("Bearer");
    }

    /**
     * Actualización de tiempo de vidada del token
     *
     * @param auth Entidad principal autenticada
     * @return Cadena con un json web token
     */
    public String createRefreshToken(Authentication auth) {
        long expire = 7200;
        String scope = auth.getAuthorities().stream()
                .map(ma -> ma.getAuthority())
                .collect(Collectors.joining(" "));

        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(Instant.now())
                .subject(auth.getName())
                .claim("typ", "Refresh")
                .expiresAt(Instant.now().plusSeconds(expire))
                .id(UUID.randomUUID().toString())
                .build();

        return jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }
}
