package com.lorena.evaluacioncliente.service.config;

import com.lorena.evaluacioncliente.dto.response.LoginDto;
import com.lorena.evaluacioncliente.dto.response.UserJwtDto;
import com.lorena.evaluacioncliente.entity.config.User;
import com.lorena.evaluacioncliente.repository.config.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Contiene endpoints para la autenticación del usuario
 *
 * @author lorenav
 */
@Service
public class AuthService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    TokenService tokenService;

    /**
     * Valida las credenciales enviadas para una autorización de uso de los enpoints del sistema
     *
     * @param username Clave del usuario
     * @param password Contraseña del usuario
     * @return Respuesta a la validación de login
     */
    public LoginDto loginUser(String username, String password) {
        try {
            User userDb = userRepository.findByUsernameOrEmail(username, username).orElseThrow(() -> new UsernameNotFoundException("Credenciales incorrectas"));
            String user = userDb.getUsername();
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user, password));
            String token = tokenService.generateJwt(auth);
            String refreshToken = null;
            if (auth.isAuthenticated()) {
                refreshToken = tokenService.createRefreshToken(auth);
            }
            UserJwtDto userRs = new UserJwtDto(userRepository.findByUsernameOrEmail(username, username).get());
            String expireToken = "3600";
            String expireRefreshToken = "7200";
            return new LoginDto(userRs, token, refreshToken, expireToken, expireRefreshToken);
        } catch (AuthenticationException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
