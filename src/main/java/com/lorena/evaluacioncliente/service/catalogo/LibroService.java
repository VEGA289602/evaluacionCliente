package com.lorena.evaluacioncliente.service.catalogo;

import com.lorena.evaluacioncliente.entity.catalogo.Libro;
import com.lorena.evaluacioncliente.repository.catalogo.LibroRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Contiene endpoints para la administración del catálogo de Libro
 *
 * @author lorenav
 */
@Service
public class LibroService {

    @Autowired
    private LibroRepository libroRepository;

    @Autowired
    private EntityManager entityManager;

    /**
     * Obtiene una lista de libros dependiendo los filtros de búsqueda
     *
     * @param parametros Mapa con los parámetros enviados en la solicitud
     * @return Lista de libros filtrados por los parámetros de búsqueda
     */
    public List<Libro> consultar(Map<String, String> parametros) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Libro> criteriaQuery = criteriaBuilder.createQuery(Libro.class);

        Root<Libro> book = criteriaQuery.from(Libro.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(book.get("estatus"), true));

        if (parametros.get("titulo") != null && !parametros.get("titulo").trim().isEmpty()) {
            predicates.add(criteriaBuilder.like(book.get("titulo"), "%" + parametros.get("titulo").trim() + "%"));
        }

        if (parametros.get("autor") != null && !parametros.get("autor").trim().isEmpty()) {
            predicates.add(criteriaBuilder.like(book.get("autor"), "%" + parametros.get("autor").trim() + "%"));
        }

        criteriaQuery.where(predicates.toArray(new Predicate[0]));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
