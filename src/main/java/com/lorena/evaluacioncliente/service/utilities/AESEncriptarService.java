package com.lorena.evaluacioncliente.service.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * Contiene funciones independientes, las cuales realizan tareas específicas para la construcción de otros componentes
 *
 * @author lorenav
 */
@Service
public class AESEncriptarService {

    @Value("${evaluacion-cliente.aes-security.standar-algorithm}")
    private String standarAlgorithm;

    @Value("${evaluacion-cliente.aes-security.vector}")
    String vector;

    @Value("${evaluacion-cliente.aes-security.secret}")
    String secret;

    /**
     * Realiza la encriptación de una cadena bajo el modelo de AES
     *
     * @param cadena Cadena a la cual se va a encriptar
     * @return Cadena modificada y encriptada
     */
    public String encriptar(String cadena) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(vector.getBytes("UTF-8"));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance(standarAlgorithm);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            byte[] encBytes = cipher.doFinal(cadena.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(encBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Realiza la desencriptación de una cadena bajo el modelo de AES
     *
     * @param cadena Cadena a la cual se va a desencriptar
     * @return Cadena original antes de encriptación
     */
    public String desencriptar(String cadena) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(vector.getBytes("UTF-8"));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance(standarAlgorithm);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            byte[] encBytes = cipher.doFinal(Base64.getDecoder().decode(cadena));
            return new String(encBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
