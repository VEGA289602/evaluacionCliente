package com.lorena.evaluacioncliente.service.apiExterno;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

/**
 * Contiene endpoints para consumir el servicio externo de pokemon
 *
 * @author lorenav
 */
@Service
public class PokemonService {
    private final WebClient webClient;

    /**
     * Constructor de la clase
     * Define la url del servicio a consumir
     */
    public PokemonService() {
        WebClient.Builder builder = WebClient.builder();
        webClient = builder.baseUrl("https://pokeapi.co/api/").build();
    }

    /**
     * Obtiene una lista de información de pokemon
     *
     * @return Mapa de objetos contenidos en el servicio externo
     */
    public Map<Object, Object> getPokemonDitto() {
        return webClient
                .get()
                .uri("v2/pokemon/ditto")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Map<Object, Object>>() {
                }).block();
    }
}
